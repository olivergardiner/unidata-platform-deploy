##There is a project for fast start up with Unidata-platform docker images.

## Fast start up

For start Unidata-platform run
```bash
docker-compose up -d
```

## Configuration

You can use it as is - with latest version of unidata-platform and unidata-platform-ui,
or set specific version in ```.env``` file.

In additional, you can set there outer ports for elastic, postgres, unidata-platform and unidata-platform-ui.

Each variable in ```.env``` has a telling name. 

## Store data

In docker-compose.yml by default we use local docker-volumes for postgres and elastic.
You can override it, and set some another local path. 

Example for postgres container:
```yaml
postgres-unidata:
    volumes:
      - /some/path/for/postgres/data:/var/lib/postgresql/data
```


